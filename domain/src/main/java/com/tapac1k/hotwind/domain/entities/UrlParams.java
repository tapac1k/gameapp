package com.tapac1k.hotwind.domain.entities;

public class UrlParams {
    private String token;
    private String version;
    private int lob;

    public UrlParams(String token, String version, int lob) {
        this.token = token;
        this.version = version;
        this.lob = lob;
    }

    public String getToken() {
        return token;
    }

    public String getVersion() {
        return version;
    }

    public int getLob() {
        return lob;
    }
}
