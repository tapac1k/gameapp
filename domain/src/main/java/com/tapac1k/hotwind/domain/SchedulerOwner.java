package com.tapac1k.hotwind.domain;

import io.reactivex.Scheduler;

public class SchedulerOwner {
    private Scheduler io;
    private Scheduler main;

    public SchedulerOwner(Scheduler io, Scheduler main) {
        this.io = io;
        this.main = main;
    }

    public Scheduler getIo() {
        return io;
    }

    public Scheduler getMain() {
        return main;
    }
}
