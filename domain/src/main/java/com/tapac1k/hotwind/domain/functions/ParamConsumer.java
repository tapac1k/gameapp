package com.tapac1k.hotwind.domain.functions;

public interface ParamConsumer<T> {
    void consume(T param);
}
