package com.tapac1k.hotwind.domain.functions;

public interface Consumer {
    void consume();
}
