package com.tapac1k.hotwind.domain;


import com.tapac1k.hotwind.domain.entities.GameAuth;
import com.tapac1k.hotwind.domain.entities.UrlParams;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Implementation of this repository should work with local data storage: DB or SharedPreferences
 * for example
 */
public interface LocalRepository {
    /**
     * Get current game authentication data like token, game url etc
     * @return Single stream that emit game auth data or error if user is logged out
     */
    Single<GameAuth> getGameAuth();

    /**
     * Get params to open game url
     * @return Single stream that emit params to open url
     */
    Single<UrlParams> getUrlParams();

    /**
     * Save GameAuth data locally
     * @return Completable stream which completes when data is successfully saved
     */
    Completable saveGameAuth(GameAuth gameAuth);

    /**
     * Method to logout user
     * @return Stream that completes on logout success
     */
    Completable logout();



}
