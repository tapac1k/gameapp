package com.tapac1k.hotwind.domain;

import com.tapac1k.hotwind.domain.entities.GameAuth;

import io.reactivex.Single;

/**
 * Implementation of this repository should work with remote API: https://web.bjhzpb.com/
 */
public interface ApiRepository {
    /**
     * Method to receive token from api
     * @param username - credentials
     * @param password- credentials
     * @return Single stream with token and information about user
     */
    Single<GameAuth> login(String username, String password);


}
