package com.tapac1k.hotwind.domain.interactors.auth;

import com.tapac1k.hotwind.domain.ApiRepository;
import com.tapac1k.hotwind.domain.LocalRepository;
import com.tapac1k.hotwind.domain.SchedulerOwner;
import com.tapac1k.hotwind.domain.interactors.core.CompletableUseCase;

import javax.inject.Inject;

import io.reactivex.Completable;

public class LoginUseCase extends CompletableUseCase<LoginUseCase.Param> {
    private ApiRepository apiRepository;
    private LocalRepository localRepository;

    @Inject
    public LoginUseCase(
            ApiRepository apiRepository,
            LocalRepository localRepository,
            SchedulerOwner owner
    ) {
        super(owner);
        this.apiRepository = apiRepository;
        this.localRepository = localRepository;
    }

    @Override
    protected Completable buildStream(Param params) {
        return apiRepository.login(
                params.username,
                params.password
        ).flatMapCompletable(gameAuth -> localRepository.saveGameAuth(gameAuth));
    }

    public static class Param {
        String username;
        String password;

        public Param(String username, String password) {
            this.username = username;
            this.password = password;
        }
    }
}
