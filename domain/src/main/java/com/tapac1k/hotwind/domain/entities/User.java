package com.tapac1k.hotwind.domain.entities;

import io.reactivex.annotations.NonNull;

public class User {
    private String id;
    private String nickname;

    public User(String id, String nickname) {
        this.id = id;
        this.nickname = nickname;
    }

    public String getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }
}
