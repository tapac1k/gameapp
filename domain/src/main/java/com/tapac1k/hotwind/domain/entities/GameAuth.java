package com.tapac1k.hotwind.domain.entities;

public class GameAuth {
    private String url;
    private String token;
    private User user;

    public GameAuth(String url, String token, User user) {
        this.url = url;
        this.token = token;
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }
}
