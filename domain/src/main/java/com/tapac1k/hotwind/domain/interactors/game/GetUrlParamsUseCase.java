package com.tapac1k.hotwind.domain.interactors.game;

import com.tapac1k.hotwind.domain.LocalRepository;
import com.tapac1k.hotwind.domain.SchedulerOwner;
import com.tapac1k.hotwind.domain.entities.UrlParams;
import com.tapac1k.hotwind.domain.interactors.core.SingleUseCase;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetUrlParamsUseCase extends SingleUseCase<Void, UrlParams> {
    private LocalRepository localRepository;

    @Inject
    public GetUrlParamsUseCase(
            LocalRepository localRepository,
            SchedulerOwner owner
    ) {
        super(owner);
        this.localRepository = localRepository;
    }

    @Override
    protected Single<UrlParams> buildStream(Void params) {
        return localRepository.getUrlParams();
    }

}
