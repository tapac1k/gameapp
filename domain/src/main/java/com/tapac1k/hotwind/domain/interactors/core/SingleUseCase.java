package com.tapac1k.hotwind.domain.interactors.core;

import com.tapac1k.hotwind.domain.SchedulerOwner;
import com.tapac1k.hotwind.domain.functions.ParamConsumer;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public abstract class SingleUseCase<IN, OUT> {
    private Scheduler mainThread;
    private Scheduler ioThread;

    public SingleUseCase(SchedulerOwner schedulerOwner) {
        this.ioThread = schedulerOwner.getIo();
        this.mainThread = schedulerOwner.getMain();
    }

    protected abstract Single<OUT> buildStream(IN params);

    public SingleObserver<OUT> execute(
            IN params,
            ParamConsumer<OUT> onSuccess,
            ParamConsumer<Throwable> onError
    ) {
        return buildStream(params)
                .subscribeOn(ioThread)
                .observeOn(mainThread)
                .subscribeWith(new SingleObserver<OUT>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(OUT out) {
                        onSuccess.consume(out);
                    }

                    @Override
                    public void onError(Throwable e) {
                        onError.consume(e);
                    }
                });
    }

}
