package com.tapac1k.hotwind.domain.interactors.game;

import com.tapac1k.hotwind.domain.LocalRepository;
import com.tapac1k.hotwind.domain.SchedulerOwner;
import com.tapac1k.hotwind.domain.entities.GameAuth;
import com.tapac1k.hotwind.domain.interactors.core.SingleUseCase;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetGameAuthDataUseCase extends SingleUseCase<Void, GameAuth> {
    private LocalRepository localRepository;

    @Inject
    public GetGameAuthDataUseCase(
            LocalRepository localRepository,
            SchedulerOwner owner
    ) {
        super(owner);
        this.localRepository = localRepository;
    }

    @Override
    protected Single<GameAuth> buildStream(Void params) {
        return localRepository.getGameAuth();
    }

}
