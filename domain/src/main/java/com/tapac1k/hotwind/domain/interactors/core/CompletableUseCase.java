package com.tapac1k.hotwind.domain.interactors.core;

import com.tapac1k.hotwind.domain.SchedulerOwner;
import com.tapac1k.hotwind.domain.functions.Consumer;
import com.tapac1k.hotwind.domain.functions.ParamConsumer;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

public abstract class CompletableUseCase<IN> {
    private Scheduler mainThread;
    private Scheduler ioThread;

    public CompletableUseCase(SchedulerOwner schedulerOwner) {
        this.ioThread = schedulerOwner.getIo();
        this.mainThread = schedulerOwner.getMain();
    }

    protected abstract Completable buildStream(IN params);

    public final CompletableObserver execute(
            IN params,
            Consumer onComplete,
            ParamConsumer<Throwable> onError
    ) {
        return buildStream(params)
                .subscribeOn(ioThread)
                .observeOn(mainThread)
                .subscribeWith(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        onComplete.consume();
                    }

                    @Override
                    public void onError(Throwable e) {
                        onError.consume(e);
                    }
                });
    }
}
