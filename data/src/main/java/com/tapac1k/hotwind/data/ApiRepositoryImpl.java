package com.tapac1k.hotwind.data;

import com.tapac1k.hotwind.data.sources.api.ApiService;
import com.tapac1k.hotwind.domain.ApiRepository;
import com.tapac1k.hotwind.domain.entities.BadCredentialException;
import com.tapac1k.hotwind.domain.entities.GameAuth;

import javax.inject.Inject;

import io.reactivex.Single;

public class ApiRepositoryImpl implements ApiRepository {
    private ApiService apiService;
    private LoginResponseToGameAuthMapper loginResponseToGameAuthMapper;

    @Inject
    public ApiRepositoryImpl(ApiService apiService,
                             LoginResponseToGameAuthMapper loginResponseToGameAuthMapper) {
        this.apiService = apiService;
        this.loginResponseToGameAuthMapper = loginResponseToGameAuthMapper;
    }

    @Override
    public Single<GameAuth> login(String username, String password) {
        return apiService.login(username, password)
                .map(a -> {
                    if(a.data == null) throw new BadCredentialException();
                    return loginResponseToGameAuthMapper.map(a.data);
                });
    }
}
