package com.tapac1k.hotwind.data.sources.api;

import com.tapac1k.hotwind.data.sources.api.entities.BaseResponse;
import com.tapac1k.hotwind.data.sources.api.entities.LoginResponse;
import com.tapac1k.hotwind.data.sources.api.entities.UserResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/home/legend/game_login")
    Single<BaseResponse<LoginResponse>> login(
            @Query("username") String username,
            @Query("password") String password
    );
}
