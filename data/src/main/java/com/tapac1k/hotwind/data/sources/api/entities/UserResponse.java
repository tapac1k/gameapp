package com.tapac1k.hotwind.data.sources.api.entities;

import com.squareup.moshi.Json;

public class UserResponse {
    @Json(name = "id")
    public String id;
    @Json(name = "nickname")
    public String nickname;
}