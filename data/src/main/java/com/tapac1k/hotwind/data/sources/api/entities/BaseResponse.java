package com.tapac1k.hotwind.data.sources.api.entities;

import com.squareup.moshi.Json;

public class BaseResponse<T> {
    @Json(name = "code")
    public int code;
    @Json(name = "data")
    public T data;
    @Json(name = "msg")
    public String msg;
}
