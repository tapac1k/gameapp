package com.tapac1k.hotwind.data;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import com.tapac1k.hotwind.domain.LocalRepository;
import com.tapac1k.hotwind.domain.entities.GameAuth;
import com.tapac1k.hotwind.domain.entities.UrlParams;
import com.tapac1k.hotwind.domain.entities.User;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;


@SuppressLint("ApplySharedPref")
public class LocalRepositoryImpl implements LocalRepository {
    private SharedPreferences sharedPreferences;
    private final static String VERSION = "1.0.0";
    private final static int LOB = 1;

    @Inject
    LocalRepositoryImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public Single<GameAuth> getGameAuth() {
        return Single.fromCallable(() -> {
            User user = new User(
                    sharedPreferences.getString("id", null),
                    sharedPreferences.getString("nickname", null)
            );
            assert user.getId() != null;
            GameAuth gameAuth = new GameAuth(
                    sharedPreferences.getString("url", null),
                    sharedPreferences.getString("token", null),
                    user
            );
            assert gameAuth.getToken() != null;
            assert gameAuth.getUrl() != null;
            return gameAuth;
        });
    }

    @Override
    public Completable saveGameAuth(GameAuth gameAuth) {
        return Completable.fromAction(() -> sharedPreferences.edit()
                .putString("token", gameAuth.getToken())
                .putString("url", gameAuth.getUrl())
                .putString("id", gameAuth.getUser().getId())
                .putString("nickname", gameAuth.getUser().getNickname())
                .commit());
    }

    @Override
    public Completable logout() {
        return Completable.fromAction(() -> sharedPreferences.edit()
                .putString("token", null)
                .putString("url", null)
                .putString("id", null)
                .putString("nickname", null)
                .commit());
    }

    @Override
    public Single<UrlParams> getUrlParams() {
        return getGameAuth().map(it -> new UrlParams(it.getToken(), VERSION, LOB));
    }
}
