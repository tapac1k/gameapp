package com.tapac1k.hotwind.data;

import com.tapac1k.hotwind.data.sources.api.entities.LoginResponse;
import com.tapac1k.hotwind.domain.entities.GameAuth;
import com.tapac1k.hotwind.domain.entities.User;

import javax.inject.Inject;

public class LoginResponseToGameAuthMapper {

    @Inject
    public LoginResponseToGameAuthMapper(){

    }

    GameAuth map(LoginResponse response) {
        return new GameAuth(
                response.url,
                response.token,
                new User(
                        response.user.id,
                        response.user.nickname
                )
        );
    }
}
