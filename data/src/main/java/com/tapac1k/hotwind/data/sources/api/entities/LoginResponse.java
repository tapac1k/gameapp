package com.tapac1k.hotwind.data.sources.api.entities;

import com.squareup.moshi.Json;

public class LoginResponse {
    @Json(name = "user")
    public UserResponse user;
    @Json(name = "token")
    public String token;
    @Json(name = "url")
    public String url;
}
