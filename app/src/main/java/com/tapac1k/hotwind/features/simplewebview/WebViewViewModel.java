package com.tapac1k.hotwind.features.simplewebview;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.tapac1k.hotwind.common.HotWindViewModel;
import com.tapac1k.hotwind.domain.interactors.game.GetUrlParamsUseCase;

import javax.inject.Inject;

public class WebViewViewModel extends HotWindViewModel<WebViewActivity.Command, WebViewActivityArgs> {
    private GetUrlParamsUseCase getUrlParamsUseCase;
    private MutableLiveData<String> url = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>(true);


    public LiveData<Boolean> getLoading() {
        return loading;
    }

    void setLoaded() {
        loading.postValue(false);
    }

    public LiveData<String> getUrl() {
        return url;
    }

    @Inject
    WebViewViewModel(GetUrlParamsUseCase getUrlParamsUseCase) {
        this.getUrlParamsUseCase = getUrlParamsUseCase;
    }

    @Override
    protected void initWithArguments(@Nullable WebViewActivityArgs args) {
        super.initWithArguments(args);
        assert args != null;
        getUrlParamsUseCase.execute(null, param -> {
            String urlWithParams = args.getUrl() + "?" +
                    "token=" + param.getToken() +
                    "&ver=" + param.getVersion() +
                    "&lob=" + param.getLob();
            url.setValue(urlWithParams);
        }, error -> {

        });
    }
}
