package com.tapac1k.hotwind.features.start;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavArgs;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.tapac1k.hotwind.R;
import com.tapac1k.hotwind.common.HotWindViewModel;
import com.tapac1k.hotwind.domain.entities.BadCredentialException;
import com.tapac1k.hotwind.domain.interactors.auth.LoginUseCase;

import javax.inject.Inject;

public class StartViewModel extends HotWindViewModel<StartFragment.Command, NavArgs> {
    public MutableLiveData<String> username = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<Boolean> loading = new MutableLiveData<>(false);

    private LoginUseCase loginUseCase;
    private Resources resources;

    @Inject
    public StartViewModel(
            LoginUseCase loginUseCase,
            Resources resources
    ) {
        this.loginUseCase = loginUseCase;
        this.resources = resources;
    }

    public void onLogin() {
        loading.setValue(true);
        notifyEvent(StartFragment.Command.HIDE_KEYBOARD);
        loginUseCase.execute(new LoginUseCase.Param(username.getValue(), password.getValue()), () -> {
            loading.setValue(false);
            notifyEvent(StartFragment.Command.NAVIGATE_GAME);
        }, error -> {
            loading.setValue(false);
            Bundle extras = new Bundle();
            if (error instanceof BadCredentialException)
                extras.putString("msg", resources.getString(R.string.bad_credentials));
            else if (error instanceof HttpException)
                extras.putString("msg", error.getLocalizedMessage());
            else extras.putString("msg", resources.getString(R.string.something_wrong));

            notifyEvent(StartFragment.Command.SHOW_TOAST, extras);
        });
    }
}
