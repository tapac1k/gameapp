package com.tapac1k.hotwind.features.start;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavArgs;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.tapac1k.hotwind.R;
import com.tapac1k.hotwind.common.HotWindFragment;
import com.tapac1k.hotwind.common.ViewModelFactory;
import com.tapac1k.hotwind.databinding.StartFragmentBinding;

import javax.inject.Inject;

public class StartFragment extends HotWindFragment<StartFragment.Command, NavArgs> {

    private StartFragmentBinding binding;
    private NavController navController;

    @Inject
    ViewModelFactory viewModelFactory;
    private StartViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        binding = StartFragmentBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(this);
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(StartViewModel.class);
        binding.setViewModel(viewModel);
        initViewModel(viewModel, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    @Override
    protected void onEvent(@NonNull Command command, @Nullable Bundle extras) {
        switch (command) {
            case NAVIGATE_GAME:
                NavDirections action = StartFragmentDirections.actionStartFragmentToGameFragment();
                navController.navigate(action);
                break;

            case SHOW_TOAST:
                assert extras != null;
                Toast.makeText(requireContext(), extras.getString("msg"), Toast.LENGTH_SHORT).show();
                break;
            case HIDE_KEYBOARD:
                hideKeyboard();
                break;
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(binding.etUsername.getWindowToken(), 0);
        }
    }

    enum Command {
        NAVIGATE_GAME,
        SHOW_TOAST,
        HIDE_KEYBOARD
    }
}
