package com.tapac1k.hotwind.features.game;

import android.content.res.Resources;

import androidx.core.os.BundleKt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavArgs;

import com.tapac1k.hotwind.R;
import com.tapac1k.hotwind.common.HotWindViewModel;
import com.tapac1k.hotwind.domain.interactors.game.GetGameAuthDataUseCase;

import javax.inject.Inject;

import kotlin.Pair;

public class GameViewModel extends HotWindViewModel<GameFragment.Command, NavArgs> {
    private MutableLiveData<String> url = new MutableLiveData<>();
    private Resources resources;
    private MutableLiveData<Boolean> loading = new MutableLiveData<>(true);


    public LiveData<Boolean> getLoading() {
        return loading;
    }

    void setLoaded() {
        loading.postValue(false);
    }

    private final static String HELP_URL = "https://web.bjhzpb.com/home/legend/help";
    private final static String CHARGE_URL = "https://web.bjhzpb.com/home/legend/deposit_chip";
    private final static String EXCHANGE_URL = "https://web.bjhzpb.com/home/legend/withdraw_chip";
    private final static String SERVICE_URL = "https://web.bjhzpb.com/home/legend/service";

    public LiveData<String> getUrl() {
        return url;
    }

    @Inject
    GameViewModel(
            GetGameAuthDataUseCase getGameAuthDataUseCase,
            Resources resources
    ) {
        this.resources = resources;
        getGameAuthDataUseCase.execute(null,
                gameAuth -> url.setValue(gameAuth.getUrl()),
                error -> notifyEvent(GameFragment.Command.BACK)
        );
    }

    @SuppressWarnings("unchecked")
    private void navigateUrl(String title, String url) {
        notifyEvent(
                GameFragment.Command.OPEN_URL,
                BundleKt.bundleOf(
                        new Pair<>("url", url),
                        new Pair<>("title", title)
                ));
    }

    void onService() {
        navigateUrl(resources.getString(R.string.service), SERVICE_URL);
    }

    void onCharge() {
        navigateUrl(resources.getString(R.string.charge), CHARGE_URL);
    }

    void onExchange() {
        navigateUrl(resources.getString(R.string.exchange), EXCHANGE_URL);
    }

    void onHelp() {
        navigateUrl(resources.getString(R.string.help), HELP_URL);
    }

    void onClose() {
        notifyEvent(GameFragment.Command.BACK);
    }

    void onHide() {
        notifyEvent(GameFragment.Command.BACK);
    }


}
