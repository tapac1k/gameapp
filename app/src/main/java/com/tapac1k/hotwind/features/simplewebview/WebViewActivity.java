package com.tapac1k.hotwind.features.simplewebview;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.tapac1k.hotwind.R;
import com.tapac1k.hotwind.common.ViewModelFactory;
import com.tapac1k.hotwind.databinding.WebviewActivityBinding;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class WebViewActivity extends AppCompatActivity {
    private WebviewActivityBinding binding;

    @Inject
    ViewModelFactory viewModelFactory;
    WebViewViewModel viewModel;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.webview_activity);
        binding.setLifecycleOwner(this);
        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                viewModel.setLoaded();
            }
        });
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        binding.webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        binding.webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(WebViewViewModel.class);
        binding.setViewModel(viewModel);
        WebViewActivityArgs args = WebViewActivityArgs.fromBundle(Objects.requireNonNull(getIntent().getExtras()));
        viewModel.initWithArguments(args);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(args.getTitle());
        binding.webView.restoreState(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        binding.webView.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    enum Command {

    }
}
