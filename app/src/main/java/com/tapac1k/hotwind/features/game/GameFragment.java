package com.tapac1k.hotwind.features.game;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavArgs;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.tapac1k.hotwind.R;
import com.tapac1k.hotwind.common.HotWindFragment;
import com.tapac1k.hotwind.common.ViewModelFactory;
import com.tapac1k.hotwind.databinding.GameFragmentBinding;

import java.util.Objects;

import javax.inject.Inject;

public class GameFragment extends HotWindFragment<GameFragment.Command, NavArgs> {
    private GameFragmentBinding binding;

    @Inject
    ViewModelFactory viewModelFactory;
    private GameViewModel viewModel;
    private NavController navController;

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = GameFragmentBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(this);
        binding.webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                viewModel.setLoaded();
            }
        });
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        binding.webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        binding.webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.addJavascriptInterface(new JavascriptInterface(), "myapp");

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        CookieManager.getInstance().setAcceptThirdPartyCookies(binding.webView, true);

        binding.webView.restoreState(savedInstanceState);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(GameViewModel.class);
        binding.setViewModel(viewModel);
        initViewModel(viewModel, null);
    }

    @Override
    protected void onEvent(@NonNull Command command, @Nullable Bundle extras) {
        switch (command) {
            case OPEN_URL:
                assert extras != null;
                NavDirections action =
                        GameFragmentDirections
                                .actionGameFragmentToWebViewActivity(
                                        Objects.requireNonNull(extras.getString("url")),
                                        Objects.requireNonNull(extras.getString("title"))
                                );
                navController.navigate(action);
                break;
            case BACK:
                requireActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        binding.webView.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    class JavascriptInterface {
        @android.webkit.JavascriptInterface
        public void GotoService(Object obj) {
            viewModel.onService();
        }

        @android.webkit.JavascriptInterface
        public void DoCharge(Object obj) {
            viewModel.onCharge();
        }

        @android.webkit.JavascriptInterface
        public void DoExchange(Object obj) {
            viewModel.onExchange();
        }

        @android.webkit.JavascriptInterface
        public void Close(Object obj) {
            viewModel.onClose();
        }

        @android.webkit.JavascriptInterface
        public void Hide(Object obj) {
            viewModel.onHide();
        }

        @android.webkit.JavascriptInterface
        public void GotoHelp(Object obj) {
            viewModel.onHelp();
        }
    }

    enum Command {
        OPEN_URL,
        BACK
    }
}
