package com.tapac1k.hotwind.di.modules;

import androidx.lifecycle.ViewModel;

import com.tapac1k.hotwind.di.ViewModelKey;
import com.tapac1k.hotwind.features.start.StartFragment;
import com.tapac1k.hotwind.features.start.StartViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class AuthModule {
    @Binds
    @IntoMap
    @ViewModelKey(StartViewModel.class)
    abstract ViewModel startViewModel(StartViewModel userViewModel);

    @ContributesAndroidInjector
    abstract StartFragment contributeStartFragment();
}
