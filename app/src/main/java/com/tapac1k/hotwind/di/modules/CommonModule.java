package com.tapac1k.hotwind.di.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.squareup.moshi.Moshi;
import com.tapac1k.hotwind.BuildConfig;
import com.tapac1k.hotwind.data.ApiRepositoryImpl;
import com.tapac1k.hotwind.data.LocalRepositoryImpl;
import com.tapac1k.hotwind.data.sources.api.ApiService;
import com.tapac1k.hotwind.domain.ApiRepository;
import com.tapac1k.hotwind.domain.LocalRepository;
import com.tapac1k.hotwind.domain.SchedulerOwner;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class CommonModule {
    private Context context;

    public CommonModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(BuildConfig.DEBUG ? Level.BODY : Level.NONE);
        return interceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Interceptor logInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Moshi provideMoshi() {
        return new Moshi.Builder().build();
    }

    @Provides
    @Singleton
    ApiService provideApiService(
            OkHttpClient client,
            Moshi moshi
    ) {
        return new Retrofit.Builder()
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://web.bjhzpb.com")
                .build()
                .create(ApiService.class);
    }

    @Provides
    @Singleton
    ApiRepository provideApiRepository(ApiRepositoryImpl repository) {
        return repository;
    }

    @Provides
    @Singleton
    LocalRepository provideLocalRepository(LocalRepositoryImpl repository) {
        return repository;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("LocalData",Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SchedulerOwner provideSchedulerOwner() {
        return new SchedulerOwner(
                Schedulers.io(),
                AndroidSchedulers.mainThread()
        );
    }

    @Provides
    Resources provideResources(Context context) {
        return context.getResources();
    }
}
