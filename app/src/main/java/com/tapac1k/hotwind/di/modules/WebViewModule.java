package com.tapac1k.hotwind.di.modules;

import androidx.lifecycle.ViewModel;

import com.tapac1k.hotwind.di.ViewModelKey;
import com.tapac1k.hotwind.features.simplewebview.WebViewActivity;
import com.tapac1k.hotwind.features.simplewebview.WebViewViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class WebViewModule {
    @Binds
    @IntoMap
    @ViewModelKey(WebViewViewModel.class)
    abstract ViewModel simpleWebViewViewModel(WebViewViewModel userViewModel);

    @ContributesAndroidInjector
    abstract WebViewActivity contributeWebViewActivity();
}
