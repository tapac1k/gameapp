package com.tapac1k.hotwind.di.modules;

import androidx.lifecycle.ViewModel;

import com.tapac1k.hotwind.di.ViewModelKey;
import com.tapac1k.hotwind.features.game.GameFragment;
import com.tapac1k.hotwind.features.game.GameViewModel;
import com.tapac1k.hotwind.features.start.StartViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class GameModule {
    @Binds
    @IntoMap
    @ViewModelKey(GameViewModel.class)
    abstract ViewModel gameViewModel(GameViewModel userViewModel);

    @ContributesAndroidInjector
    abstract GameFragment contributeGameFragment();
}
