package com.tapac1k.hotwind.di.component;

import com.tapac1k.hotwind.HotWindApp;
import com.tapac1k.hotwind.di.modules.AuthModule;
import com.tapac1k.hotwind.di.modules.CommonModule;
import com.tapac1k.hotwind.di.modules.GameModule;
import com.tapac1k.hotwind.di.modules.WebViewModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        CommonModule.class,
        AuthModule.class,
        GameModule.class,
        WebViewModule.class
})
public interface AppComponent {

    void inject(HotWindApp hotWindApp);
}