package com.tapac1k.hotwind.common;

import android.webkit.WebView;

import androidx.databinding.BindingAdapter;

public class BindingAdapters {
    @BindingAdapter("android:initialUrl")
    public static void setUrl(WebView webView, String url) {
        if(webView.getUrl() != null) return;
        webView.loadUrl(url);
    }
}
