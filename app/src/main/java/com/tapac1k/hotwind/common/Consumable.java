package com.tapac1k.hotwind.common;

import android.os.Bundle;

class Consumable<T> {
    private T data;
    private Bundle extras;

    private boolean consumed = false;

    Consumable(T data, Bundle extras) {
        this.data = data;
        this.extras = extras;
    }

    Consumable(T data) {
        this.data = data;
    }

    Bundle getExtras() {
        return extras;
    }

    T getNotHandledData() {
        if(consumed) return null;
        consumed = true;
        return data;
    }
}
