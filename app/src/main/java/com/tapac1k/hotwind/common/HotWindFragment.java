package com.tapac1k.hotwind.common;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavArgs;

import dagger.android.support.AndroidSupportInjection;

public abstract class HotWindFragment<Event extends Enum<Event>, Args extends NavArgs>
        extends Fragment {
    protected void onEvent(@NonNull Event event, @Nullable Bundle extras) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }

    protected void initViewModel(@NonNull HotWindViewModel<Event, Args> viewModel, @Nullable Args args) {
        viewModel.initialize(args);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventConsumable -> {
            Event event = eventConsumable.getNotHandledData();
            try {
                if (event != null) onEvent(event, eventConsumable.getExtras());
            } catch (Exception e) {
                //Collect navigation and events warning
            }
        });
    }
}
