package com.tapac1k.hotwind.common;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.navigation.NavArgs;

public abstract class HotWindViewModel<Event extends Enum<Event>, Args extends NavArgs> extends ViewModel {
    private MutableLiveData<Consumable<Event>> mutableEvent = new MutableLiveData<>();

    private boolean initialized = false;

    @NonNull
    LiveData<Consumable<Event>> getEvents() {
        return mutableEvent;
    }

    void initialize(@Nullable Args args) {
        if(initialized) return;
        initialized = true;
        initWithArguments(args);
    }

    protected void notifyEvent(@NonNull Event event, @Nullable Bundle extras) {
        mutableEvent.postValue(new Consumable<>(event,extras));
    }

    protected void notifyEvent(@NonNull Event event) {
        mutableEvent.postValue(new Consumable<>(event));
    }

    protected void initWithArguments(@Nullable Args args) {

    }
}
