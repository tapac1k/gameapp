package com.tapac1k.hotwind;

import android.app.Application;

import com.tapac1k.hotwind.di.component.DaggerAppComponent;
import com.tapac1k.hotwind.di.modules.CommonModule;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;


public class HotWindApp extends Application implements HasAndroidInjector {

    @Inject
    public DispatchingAndroidInjector<Object> activityInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent
                .builder()
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return activityInjector;
    }
}
